#include <iostream>
#include "Game.hpp"

//////////////////////////////////
// Constructors/Deconstructors
//////////////////////////////////

Game::Game::Game()
{
	
}

Game::Game::Game(std::string title, int width, int height, uint32_t style)
{
	this->_gameTitle = title;
	this->_windowStyle = style;
	this->_videoMode = sf::VideoMode::VideoMode(width, height);
}

Game::Game::~Game()
{

}

//////////////////////////////////
// Accessor methods
//////////////////////////////////

std::string Game::Game::getGameTitle()
{
	return this->_gameTitle;
}

//////////////////////////////////
// Public methods
//////////////////////////////////

void Game::Game::run()
{
	this->_renderWindow = new sf::RenderWindow(this->_videoMode, this->_gameTitle, this->_windowStyle);
	this->_gameWindow = this->_renderWindow->getSystemHandle();

	while (this->_renderWindow->isOpen()) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			this->_renderWindow->close();
		}
		this->_renderWindow->clear();
		this->_renderWindow->display();
	}
}