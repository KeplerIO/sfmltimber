#pragma once
#include <SFML/Graphics.hpp>

namespace Game
{
	class Game
	{
		sf::RenderWindow *_renderWindow;
		sf::WindowHandle _gameWindow;
		std::string _gameTitle;
		uint32_t _windowStyle;
		sf::VideoMode _videoMode;
		

		public:
			Game();
			Game(std::string title, int width, int height, uint32_t style);
			void run();
			std::string getGameTitle();
			~Game();
	};
}

