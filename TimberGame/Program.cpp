#include <iostream>
#include <sstream>
#include <SFML/Graphics.hpp>
#include "Game/Game.hpp"

int main()
{
/***************************************************************************/
	//Game resources cache root location
	const std::string cacheDir = "Game/Res";

	//Fonts cache location
	const std::string fontsLoc = "/fonts/";

	//Graphics cache location
	const std::string graphicsLoc = "/graphics/";

	//Sounds cache location
	const std::string soundsLoc = "/sounds/";
/***************************************************************************/
	//Decalare a video mode to be passed to a window
	sf::VideoMode vm(1920, 1080);
	//Create window with video mode, title, and style
	sf::RenderWindow window(vm, "Timber!!!", sf::Style::Fullscreen);
/***************************************************************************/
	//Load background texture into memory
	sf::Texture backgroundTexture;
	backgroundTexture.loadFromFile(cacheDir + graphicsLoc + "background.png");

	//Create sprite for texture and set initial position
	sf::Sprite backgroundSprite;
	backgroundSprite.setTexture(backgroundTexture);
	backgroundSprite.setPosition(0, 0);
/***************************************************************************/
	//Load tree trunk texture into memory
	sf::Texture treeTrunkTexture;
	treeTrunkTexture.loadFromFile(cacheDir + graphicsLoc + "tree-trunk.png");

	//Create sprite for texture and set initial position
	sf::Sprite treeTrunkSprite;
	treeTrunkSprite.setTexture(treeTrunkTexture);
	treeTrunkSprite.setPosition(810, 0);
/***************************************************************************/
	//Load bee texture into memory
	sf::Texture beeTexture;
	beeTexture.loadFromFile(cacheDir + graphicsLoc + "bee.png");

	//Create sprite for texture and set initial position
	sf::Sprite beeSprite;
	beeSprite.setTexture(beeTexture);
	beeSprite.setPosition(0, 800);

	//Extra bee properties
	bool beeActive = false;
	float beeSpeed = 0.0f;
/***************************************************************************/
	//Load cloud texture into memory
	sf::Texture cloudTexture;
	cloudTexture.loadFromFile(cacheDir + graphicsLoc + "cloud.png");

	//Create MULTIPLE sprites for texture and set initial positions
	sf::Sprite cloud1Sprite;
	sf::Sprite cloud2Sprite;
	sf::Sprite cloud3Sprite;
	cloud1Sprite.setTexture(cloudTexture);
	cloud2Sprite.setTexture(cloudTexture);
	cloud3Sprite.setTexture(cloudTexture);
	cloud1Sprite.setPosition(0, 0);
	cloud2Sprite.setPosition(0, 250);
	cloud3Sprite.setPosition(0, 500);

	//Extra properties for all clouds
	bool cloud1Active = false;
	bool cloud2Active = false;
	bool cloud3Active = false;
	float cloud1Speed = 0.0f;
	float cloud2Speed = 0.0f;
	float cloud3Speed = 0.0f;
/***************************************************************************/
	/* Game loop vars */

	//Clock for timing time between loops
	sf::Clock clock;
	
	//Vars for game score and pause state
	int score = 0;
	bool isPaused = true;

	//Text objects for on-screen message and score
	sf::Text messageText;
	sf::Text scoreText;

	//Load font into memory for on-screen message and score
	sf::Font font;
	font.loadFromFile(cacheDir + fontsLoc + "KOMIKAP_.ttf");

	//Assign loaded font to text objects
	messageText.setFont(font);
	scoreText.setFont(font);

	//Set initial text value, size and color of on-screen message text and score text
	messageText.setString("Press Enter to start!");
	scoreText.setString("Score = 0");
	messageText.setCharacterSize(75);
	scoreText.setCharacterSize(100);
	messageText.setFillColor(sf::Color::White);
	scoreText.setFillColor(sf::Color::White);

	//Instantiate a bar to be used as the game timer.
	//Declare some extra vars to track certain properties of the bar
	float timeBarStartWidth = 400;
	float timeBarHeight = 80;
	float timeRemaining = 6.0f;
	float timeBarWidthPerSecond = timeBarStartWidth / timeRemaining;
	sf::RectangleShape timeBar;
	timeBar.setSize(sf::Vector2f(timeBarStartWidth, timeBarHeight));
	timeBar.setFillColor(sf::Color::Red);
	timeBar.setPosition((1920 / 2) - timeBarStartWidth / 2, 980);

	//TODO: Use this for something?
	//sf::Time gameTimeTotal;
/***************************************************************************/
	/* Game loop */
	while(window.isOpen())
	{
		/*
		****************************************
		Handle the players input
		****************************************
		*/
		//Escape stops game
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			window.close();
		}

		//Enter UN-Pauses
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
		{
			isPaused = false;
			score = 0;
			//This is needed because if the game ends, and the user presses enter again, the time will continue to count negative and make the timer bar progress the wrong way!!
			//Comment it out to see what I mean lol
			timeRemaining = 5;
		}
		/*
		****************************************
		Update the scene
		****************************************
		*/
		//Use a rectangle to messure the bounds of the on-screen message text, then, use the measurements
		//to move the origin of the text object to it's center and position it based on that origin.
		sf::FloatRect textRect = messageText.getLocalBounds();
		messageText.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
		messageText.setPosition(1920 / 2.0f, 1080 / 2.0f);
		
		//Position the score text with a bit of padding away from the edges of the screen
		scoreText.setPosition(20, 20);

		/*
			The game starts paused and will only run the LOC above to set the positions of the score and
			on-screen message text. When the game becomes un-paused (player presses enter to start), the below if will run
			and work to update the screen during gameplay. If the game becomes paused again (game over), the text objects will
			still maintain their positions based on the code above running on every iteration, but I can amipulate the text value of them
			later below if needed
		*/
		if(!isPaused)
		{
			//Get the time that has passed since the last loop iteration
			sf::Time delta = clock.restart();

			//Subtract the time that has passed from the timeRemaining variable
			timeRemaining -= delta.asSeconds();
			
			//Resize the timebar passed on the new amount of time remaining
			timeBar.setSize(sf::Vector2f((timeBarWidthPerSecond * timeRemaining), timeBarHeight));

			//If the player has run out of time: pause the game, change the on-screen message text, re-calc the middle of the text object's
			//origin to that location and position it based on that origin
			if (timeRemaining <= 0.0f)
			{
				isPaused = true;
				messageText.setString("Out of time!!");
				sf::FloatRect textRect = messageText.getLocalBounds();
				messageText.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
				messageText.setPosition(1920 / 2.0f, 1080 / 2.0f);
			}

			/* Game object states start disabled. When the game first runs, speed and position are psudo-randomly calculated and assigned to them. 
			   On the next iteration of the loop, those values are used to update the objects.
			*/

			//Bee (Moves right to left)
			if (!beeActive)
			{
				//How fast is the bee?
				srand((int)time(0));
				beeSpeed = (rand() % 200) + 200;

				//How high is the bee?
				srand((int)time(0) * 10);
				float height = (rand() % 500) + 500;
				beeSprite.setPosition(2000, height);

				//Activate!
				beeActive = true;
			}
			else
			{
				//Multiply the desired speed of the bee by the amount of time that has passed since the last loop iteration, and move the bee that many pixels
				float xBeeLoc = (beeSprite.getPosition().x - (beeSpeed * delta.asSeconds()));
				//The height of the bee in regards to the game screen never changes until a new bee is created
				float yBeeLoc = beeSprite.getPosition().y;
				//Set the bee's position to the above calculated coords
				beeSprite.setPosition(xBeeLoc, yBeeLoc);

				//If the bee has exited the left of the screen, deactivate. On the next loop iteration, the bee will be re-initialized with new property values.
				if (beeSprite.getPosition().x < -100)
				{
					beeActive = false;
				}
			}

			//Cloud1 (Moves right to left)
			if (!cloud1Active) {
				//How fast is the cloud
				srand((int)time(0) * 10);
				cloud1Speed = (rand() % 200);

				//How high is the cloud
				srand((int)time(0) * 10);
				float height = (rand() % 150);
				cloud1Sprite.setPosition(-200, height);
				//Activate!
				cloud1Active = true;
			}
			else
			{
				//Multiply the desired speed of the cloud by the amount of time that has passed since the last loop iteration, and move the cloud that many pixels
				float cloud1XLoc = (cloud1Sprite.getPosition().x + (cloud1Speed * delta.asSeconds()));
				//The height of the cloud in regards to the game screen never changes until a new cloud is created
				float cloud1YLoc = (cloud1Sprite.getPosition().y);
				//Set the cloud's position to the above calculated coords
				cloud1Sprite.setPosition(cloud1XLoc, cloud1YLoc);

				//If the bee has exited the right of the screen, deactivate. On the next loop iteration, the cloud will be re-initialized with new property values.
				if (cloud1Sprite.getPosition().x > 1920)
				{
					cloud1Active = false;
				}

			}

			// Cloud2 (Moves right to left)
			if (!cloud2Active)
			{
				srand((int)time(0) * 20);
				cloud2Speed = (rand() % 200);

				srand((int)time(0) * 20);
				float height = (rand() % 300) - 150;
				cloud2Sprite.setPosition(-200, height);

				cloud2Active = true;
			}
			else
			{
				float cloud2XLoc = (cloud2Sprite.getPosition().x + (cloud2Speed * delta.asSeconds()));
				float cloud2YLoc = cloud2Sprite.getPosition().y;
				cloud2Sprite.setPosition(cloud2XLoc, cloud2YLoc);

				if (cloud2Sprite.getPosition().x > 1920)
				{
					cloud2Active = false;
				}
			}

			//Cloud3 (Moves right to left)
			if (!cloud3Active)
			{
				srand((int)time(0) * 30);
				cloud3Speed = (rand() % 200);
				
				srand((int)time(0) * 30);
				float height = (rand() % 450) - 150;
				cloud3Sprite.setPosition(-200, height);
				
				cloud3Active = true;
			}
			else
			{
				float cloud3XLoc = (cloud3Sprite.getPosition().x + (cloud3Speed * delta.asSeconds()));
				float cloud3YLoc = cloud3Sprite.getPosition().y;
				cloud3Sprite.setPosition(cloud3XLoc, cloud3YLoc);
				if (cloud3Sprite.getPosition().x > 1920)
				{
					cloud3Active = false;
				}
			}

			//Update the score text
			std::stringstream ss;
			ss << "Score = " << score;
			scoreText.setString(ss.str());
		}

		/*
		****************************************
		Draw the scene
		****************************************
		*/
		//Clear everything from the last frame
		window.clear();
		//Draw our game scene here (ORDER MATTERS)
		window.draw(backgroundSprite);
		window.draw(treeTrunkSprite);
		window.draw(cloud1Sprite);
		window.draw(cloud2Sprite);
		window.draw(cloud3Sprite);
		window.draw(beeSprite);
		window.draw(scoreText);
		window.draw(timeBar);

		//Only draw the on-screen message text if the game is paused (After launch or on game-over)
		if(isPaused)
		{
			window.draw(messageText);
		}

		//Show everything we drew to the buffer!
		window.display();
	}
	
	//END
	return 0;
}